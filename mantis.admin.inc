<?php
/**
 * @file 
 * Administrative functionality for mantis module.
 */

/**
 * Form constructor sets parameter for connection to Mantis Bug Tracker web
 * services.
 */
function mantis_settings_connection() {
  $form['mantis_connection_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Web Services Connnection.'),
    '#description' => t('Set the parameter for connection to your Mantis Bug Tracker installation.'),
  );
  $form['mantis_connection_fieldset']['mantis_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Mantis URL'),
    '#description' => t('URL where Mantis Bug Tracker is available. Example: http://yoursite.com/mantis/'),
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => TRUE,
    '#default_value' => variable_get('mantis_url', 'http://localhost/mantis'),
  );
  $form['mantis_connection_fieldset']['mantis_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Mantis user'),
    '#description' => t('User for connect to Mantis Bug Tracker.'),
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => TRUE,
    '#default_value' => variable_get('mantis_user', 'user'),
  );
  $form['mantis_connection_fieldset']['mantis_password'] = array(
    '#type' => 'password',
    '#title' => t('Mantis password'),
    '#description' => t('Password for connect to Mantis Bug Tracker.'),
    '#size' => 40,
    '#maxlength' => 120,
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
