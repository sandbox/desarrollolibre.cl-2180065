Mantis Bug Tracker [1]
------------------

MantisBT is an open source issue tracker that provides a delicate balance 
between simplicity and power. Users are able to get started in minutes and 
start managing their projects while collaborating with their teammates and 
clients effectively.


Mantis BT module for Drupal 7
-----------------------------

This module born like a proposal for integrate Mantis Bug Tracker and 
Drupal through a SOAP API that Mantis BT provides which is accessible at URI
http://<server-mantis>/mantis/api/soap/mantisconnect.php. 

Requirements
------------

This module requires the following modules:
* Views (https://drupal.org/project/views)
* Context (https://drupal.org/project/context)
* wsclient_soap (https://drupal.org/project/wsclient)
* Google Chart Tools (https://drupal.org/project/google_chart_tools)

How to use
----------

When you install 'mantis' module, the first step in order by to
integrate both plataforms is set the parameter for connection to your Mantis Bug 
Tracker plataform. For those you must go to: 

  Administration > Configuration > Mantis Bug Tracker > Settings Mantis Bug 
Tracker interaction

Here you'll see a form that request you three NECESSARY parameter:

* Mantis URL: URL where Mantis Bug Tracker is available.
* Mantis user: User for connect to Mantis Bug Tracker.
* Mantis password (is it necessary to explain? :-) )

Now that you have set you Mantis parameters you can see a list of projects and
issues at 

  http://<server>/<mantis-directory>?q=mantisbt/mantis-bt-project/view/

or if you have enabled clean urls 

  http://<server>/<mantis-directory>/mantisbt/mantis-bt-project/view/


You also can to generate your own views (although some limited for the moment)
through of menu:

  Structure > Views > Add new views
  
Here you must select "Mantis Bt projects" or "Mantis BT Issues" at "Show" field.

For now you can configure the next section in the views:

  * Field: Add/delete field to show in the view.

  * Sort Criteria: you can order all field whose type is "text". Other type hasn't
  implemented yet.

  * Pager: you can configurate a pager for the list.

  * If you selected "Mantis Bt projects", you can enabled/disabled the projects to
  show. You must do it at

     Advance > Query settings.

Thanks for your attention! :-)

[1] http://www.mantisbt.org/
