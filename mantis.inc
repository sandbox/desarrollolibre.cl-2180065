<?php
define('MANTIS_WSDL', '/api/soap/mantisconnect.php?wsdl');

define('MANTIS_OBJECTREF_DATA_TYPE', 'ObjectRef');
define('MANTIS_OBJECTREF_DATA_TYPE_ARRAY', 'ObjectRefArray');
define('MANTIS_ACCOUNT_DATA_TYPE', 'AccountData');
define('MANTIS_ACCOUNT_DATA_TYPE_ARRAY', 'AccountDataArray');
define('MANTIS_USER_DATA_TYPE', 'UserData');
define('MANTIS_ATTACHMENT_DATA_TYPE', 'AttachmentData');
define('MANTIS_ATTACHMENT_DATA_TYPE_ARRAY', 'AttachmentDataArray');
define('MANTIS_PROJECT_ATTACMENT_DATA_TYPE', 'ProjectAttachmentData');
define('MANTIS_RELATIONSHIP_DATA_TYPE', 'RelationshipData');
define('MANTIS_RELATIONSHIP_DATA_TYPE_ARRAY', 'RelationshipDataArray');
define('MANTIS_ISSUE_NOTE_DATA_TYPE', 'IssueNoteData');
define('MANTIS_ISSUE_NOTE_DATA_TYPE_ARRAY', 'IssueNoteDataArray');
define('MANTIS_ISSUE_DATA_TYPE', 'IssueData');
define('MANTIS_HISTORY_DATA_TYPE', 'HistoryData');
define('MANTIS_ISSUE_HEADER_DATA_TYPE', 'IssueHeaderData');
define('MANTIS_PROJECT_DATA_TYPE', 'ProjectData');
define('MANTIS_PROJECT_DATA_TYPE_ARRAY', 'ProjectDataArray');
define('MANTIS_PROJECT_VERSION_DATA_TYPE', 'ProjectVersionData');
define('MANTIS_FILTER_DATA_TYPE', 'FilterData');
define('MANTIS_CUSTOM_FIELD_DEFINITION_DATA_TYPE', 'CustomFieldDefinitionData');
define('MANTIS_CUSTOM_FIELD_LINK_PROJECT_DATA_TYPE', 'CustomFieldLinkForProjectData');
define('MANTIS_CUSTOM_FIELD_VALUE_ISSUE_DATA_TYPE', 'CustomFieldValueForIssueData');
define('MANTIS_CUSTOM_FIELD_VALUE_ISSUE_DATA_TYPE_ARRAY', 'CustomFieldValueForIssueDataArray');
define('MANTIS_TAG_DATA_TYPE', 'TagData');
define('MANTIS_TAG_DATA_SEARCH_RESULT_DATA_TYPE', 'TagDataSearchResult');
define('MANTIS_PROFILE_DATA_TYPE', 'ProfileData');
define('MANTIS_PROFILE_DATA_SEARCH_RESULT_DATA_TYPE', 'ProfileDataSearchResult');
define('MANTIS_INTEGER_DATA_TYPE', 'integer');
define('MANTIS_TEXT_DATA_TYPE', 'text');
define('MANTIS_BOOLEAN_DATA_TYPE', 'boolean');
define('MANTIS_DATE_TIME_DATA_TYPE', 'dateTime');

/**
 * Class representing web service connection to Mantis Bug Tracker.
 */
class mantis {
  protected $mantis_user;
  protected $mantis_password;
  protected $mantis_url;

  public $projects;
  public $project_active;
  public $issues;

  public function __construct($user, $password, $url) {
    $this->mantis_user = $user;
    $this->mantis_password = $password;
    $this->mantis_url = $url;
  }

  protected function get_soap_connection() {
    static $mantis_connection = NULL;
    if (!$mantis_connection) {
      $mantis_connection = new WSClientServiceDescription();
      $mantis_connection->name = 'mantis';
      $mantis_connection->label = 'Mantis Bug Tracker';
      $mantis_connection->url = $this->mantis_url . MANTIS_WSDL;
      $mantis_connection->type = 'soap';
      try {
        $mantis_connection->endpoint()->initializeMetaData();
      }
      catch (WSClientException $e) {
        throw new WSClientException($e);
      }
    }
    return $mantis_connection;
  }

  public function exec_soap_operation($operation = NULL, $arguments = array()) {
    try {
      $data = array();
      if (self::are_set_connection_parameters()) {
        $arguments['username'] = $this->mantis_user;
        $arguments['password'] = $this->mantis_password;

        $service = self::get_soap_connection();
        $data = $service->invoke($operation, $arguments);

      }
      return $data;
    }
    catch (WSClientException $e) {
      throw new WSClientException($e);
    }
    catch(Exception $e) {
      throw new Exception($e);
    }
  }

  /**
   * Returns info about the data types of the Mantis web service.
   */
  public function get_data_types() {
    try {
      $service = self::get_soap_connection();
      return $service->dataTypes();
    }
    catch(Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function get_operations() {
    try {
      $service = self::get_soap_connection();
      return $service->actions();
    }
    catch(Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  public function get_label_operations() {
    try {
      $label_operations = array();
      $soap_operations = self::getOperations();
      foreach ($soap_operations as $key => $value) {
        $label_operations[] = $key;
      }
      return $label_operations;
    }
    catch(Exception $e) {
      throw new Exception($e->getMessage());
    }
  }

  protected function are_set_connection_parameters() {
    if ($this->mantis_url && $this->mantis_user && $this->mantis_password) {
      return TRUE;
    }
    else {
      throw new Exception(t('You have not configured parameter for Mantis BT connection.'));
    }
  }

  public function get_projects_user_accessible() {
    try {
      if (!isset($this->projects)) {
        $this->projects = $this->exec_soap_operation('mc_projects_get_user_accessible');
      }
      return $this->projects;
    }
    catch (WSClientException $e) {
      throw new WSClientException($e);
    }
    catch(Exception $e) {
      throw new Exception($e);
    }
  }

  /**
   * Get a Mantis BT project by its ID. Also set the property 'project_active'
   * in this class.
   */
  public function get_project_by_id($settings = array()) {
    if (!isset($settings['id'])) {
      throw new Exception(t('Whats about "id" of Mantis BT project?'));
    }

    if (!isset($this->projects)) {
      try {
        $this->projects = $this->exec_soap_operation('mc_projects_get_user_accessible');
      }
      catch (WSClientException $e) {
        throw new WSClientException($e);
      }
    }
    $this->project_active =$this->search_project_by_id($settings, $this->projects);

    if (isset($settings['load_issues']) && $settings['load_issues']) {
      $this->get_issues();
    }
    return $this->project_active;
  }

  /**
   * Iterate over tree Mantis BT projects searching inner subprojects by 'id'.
   */
  protected function search_project_by_id($settings = array(), $mantis_bt_projects = array()) {
    foreach ($mantis_bt_projects as $project) {
      if ($project->id == $settings['id']) {
        return $project;
      }
      if (count($project->subprojects)) {
        return $this->search_project_by_id($settings, $project->subprojects);
      }
    }
    return NULL;
  }

  /**
   * Gets issues from Mantis BT Project. If 'project_id' is not set in 
   * $settings array then will be got from $this->project_active.
   */
  public function get_issues_by_project_id($settings = array()) {
    try {
      if (isset($settings['project_id'])) {
        $arguments = array(
          'project_id' => $settings['project_id'],
          'page_number' => 1, 'per_page' => 1000,
        );
        $this->issues = $this->exec_soap_operation('mc_project_get_issues', $arguments);
      }
      elseif ($this->project_active) {
        $arguments = array(
          'project_id' => $this->project_active->id,
          'page_number' => 1, 'per_page' => 1000,
        );
        $this->issues = $this->exec_soap_operation('mc_project_get_issues', $arguments);
      }
      else {
        $this->issues = NULL;
      }
      return $this->issues;
    }
    catch (WSClientException $e) {
      throw new WSClientException($e);
    }
    catch(Exception $e) {
      throw new Exception($e);
    }
  }

  public function get_issues($settings = array()) {
    try {
      return $this->get_issues_by_project_id($settings);
    }
    catch (WSClientException $e) {
      throw new WSClientException($e);
    }
    catch(Exception $e) {
      throw new Exception($e);
    }
  }
}
