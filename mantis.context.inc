<?php
/**
 * @file 
 * mantis.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function mantis_context_default_contexts() {
  $export = array();   $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'context_mantisbt_project_view_init';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'mantisbt/mantis-bt-project/view' => 'mantisbt/mantis-bt-project/view',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-mantis_bt_issues-block_1' => array(
          'module' => 'views',
          'delta' => 'mantis_bt_issues-block_1',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-mantis_bt_projects-block_1' => array(
          'module' => 'views',
          'delta' => 'mantis_bt_projects-block_1',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['context_mantisbt_project_view_init'] = $context;   $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'context_mantisbt_project_view';
  $context->description = 'Mantis BT Project View';
  $context->tag = '';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'mantisbt/mantis-bt-project/view/*' => 'mantisbt/mantis-bt-project/view/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'mantis-mantisbt_project_header' => array(
          'module' => 'mantis',
          'delta' => 'mantisbt_project_header',
          'region' => 'content',
          'weight' => '-11',
        ),
        'mantis-mantisbt_project_chart' => array(
          'module' => 'mantis',
          'delta' => 'mantisbt_project_chart',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-mantis_bt_issues-block_1' => array(
          'module' => 'views',
          'delta' => 'mantis_bt_issues-block_1',
          'region' => 'content',
          'weight' => '-9',
        ),
        'views-mantis_bt_projects-block_1' => array(
          'module' => 'views',
          'delta' => 'mantis_bt_projects-block_1',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;   // Translatables
  // Included for use with string extractors like potx.
  t('Mantis BT Project View');
  $export['context_mantisbt_project_view'] = $context;   return $export;
}
