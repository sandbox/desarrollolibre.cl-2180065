<?php
/**
 * @file
 * Base field handler ObjectRef for mantis.
 */

class mantis_object_ref_handler_field extends views_handler_field {
  /**
   * Render.
   */
  function render($values) {
    $value = $this->get_value($values);
    return $this->sanitize_value($value->name);
  }

  /**
   * Called to determine what to tell the clicksorter.
   */
  function click_sort($order) {
    if (isset($this->field_alias)) {
      $this->query->add_orderby(NULL, $this->field_alias, $order);
    }
  }

}
