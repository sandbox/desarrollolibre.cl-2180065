<?php
/**
 * @file
 * Base sort handler for mantis.
 */

class mantis_handler_sort_none extends views_handler_sort {
  /**
   * Called to add the sort to a query.
   */
  function query() {
    $this->query->add_orderby($this);
  }

  /**
   * Sort.
   */
  function sort(&$result) {
  }

  /**
   * Provide a list of options for the default sort form.
   * Should be overridden by classes that don't override sort_form
   */
  function sort_options() {
    return array();
  }

  /**
   * Determine if a sort can be exposed.
   */
  function can_expose() {
    return FALSE;
  }

  /**
   * Shortcut to display the expose/hide button.
   */
  function show_expose_button(&$form, &$form_state) {
  }

  /**
   * Provide the basic form which calls through to subforms.
   * If overridden, it is best to call through to the parent,
   * or to at least make sure all of the functions in this form
   * are called.
   */
  function options_form(&$form, &$form_state) {
  }
}
