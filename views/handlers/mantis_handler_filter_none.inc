<?php

/**
 * @file
 * Definition of views_handler_filter_none.
 */

/**
 * @ingroup views_filter_handlers
 */
class mantis_handler_filter_none extends views_handler_filter {

  /**
   * Build strings from the operators() for 'select' options
   */
  function operator_options($which = 'title') {
    $options = array();
    return $options;
  }

  /**
   * Shortcut to display the expose/hide button.
   */
  function show_expose_button(&$form, &$form_state) {
  }

  /**
   * Provide the basic form which calls through to subforms.
   * If overridden, it is best to call through to the parent,
   * or to at least make sure all of the functions in this form
   * are called.
   */
  function options_form(&$form, &$form_state) {
  }
}
