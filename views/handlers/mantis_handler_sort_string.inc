<?php
/**
 * @file
 * Base sort handler string for mantis.
 */

class mantis_handler_sort_string extends views_handler_sort {

  /**
   * Called to add the sort to a query.
   */
  function query() {
    $this->query->add_orderby($this);
  }

  /**
   * Sort.
   */
  function sort(&$result) {
    if (strtolower($this->options['order']) == 'asc') {
      usort($result, array($this, 'sort_asc'));
    }
    else {
      usort($result, array($this, 'sort_desc'));
    }
  }

  /**
   * Sort Ascending.
   */
  function sort_asc($a, $b) {
    $a_value = isset($a->{$this->field}) ? $a->{$this->field} : '';
    $b_value = isset($b->{$this->field}) ? $b->{$this->field} : '';
    return strcasecmp($a_value, $b_value);
  }

  /**
   * Sort Descending.
   */
  function sort_desc($a, $b) {
    $a_value = isset($a->{$this->field}) ? $a->{$this->field} : '';
    $b_value = isset($b->{$this->field}) ? $b->{$this->field} : '';
    return -strcasecmp($a_value, $b_value);
  }

}
