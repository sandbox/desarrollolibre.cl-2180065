<?php
/**
 * @file
 * Query plugin for mantis_bug_tracker.
 */

class mantis_issue_views_plugin_query extends views_plugin_query {
  /**
   * Builds the necessary info to execute the query.
   */
  function build(&$view) {
    $view->init_pager();
    $this->pager->query();
  }

  /**
   * Add field.
   */
  function add_field($table_alias, $real_field) {
    return $real_field;
  }

  /**
  * Add where. Class views_handler_filter invoke this function.
  */
  function add_where(){
  }

  /**
   * Execute.
   */
  function execute(&$view) {
    try {
      // @todo: To implement this solution with argument handler
      $args = arg();
      $id_project = isset($args[3]) ? $args[3] : (isset($args[2]) ? $args[2] : 0);

      $start = microtime(TRUE);

      // Avoid notices about $view->execute_time being undefined if the query
      // doesn't finish.
      $view->execute_time = NULL;

      try{
        $mantis = new mantis(variable_get('mantis_user', 'mantis'), variable_get('mantis_password', 'mantis'), variable_get('mantis_url', 'http://localhost/mantis'));
        $mantis_issues = $mantis->get_issues(array('project_id' => $id_project));
      }
      catch (WSClientException $e) {
        drupal_set_message('Impossible connect to Mantis Bug Tracker. Please check your connection parameter in Administration > Configuration >  Mantis Bug Tracker', 'error');
        return '';
      }
      $result = array();

      $result = $mantis_issues;

      if (!empty($this->orderby)) {
        // Array reverse, because the most specific are first.
        // At the moment only order by string fields.
        foreach (array_reverse($this->orderby) as $orderby) {
          $orderby->sort($result);
        }
      }

      // Pager.
      if ($this->pager->use_count_query() || !empty($view->get_total_rows)) {
        $this->pager->total_items = count($result);
        if (!empty($this->pager->options['offset'])) {
          $this->pager->total_items -= $this->pager->options['offset'];
        }

        $this->pager->update_page_info();
      }
      $offset = !empty($this->offset) ? intval($this->offset) : 0;
      $limit = !empty($this->limit) ? intval($this->limit) : 0;
      $result = $limit ? array_slice($result, $offset, $limit) : array_slice($result, $offset);

      $view->result = $result;
      $view->total_rows = count($result);
      $this->pager->post_execute($view->result);
      $view->execute_time = microtime(TRUE) - $start;
    }
    catch(Exception $e){
      drupal_set_message('An error has been found in execute (issue plugin query)');
      return;
    }
  }

  /**
   * Add Order By.
   */
  function add_orderby($orderby) {
    $this->orderby[] = $orderby;
  }
}
