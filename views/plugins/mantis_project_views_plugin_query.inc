<?php
/**
 * @file
 * Query plugin for mantis_bug_tracker.
 */

class mantis_project_views_plugin_query extends views_plugin_query {
  /**
   * Builds the necessary info to execute the query.
   */
  function build(&$view) {
    $view->init_pager();
    // Let the pager modify the query to add limits.
    $this->pager->query();
  }

  /**
   * Add field.
   */
  function add_field($table_alias, $real_field) {
    return $real_field;
  }

  /**
   * Execute.
   */
  function execute(&$view) {
    try {
      $start = microtime(TRUE);

      // Avoid notices about $view->execute_time being undefined if the query
      // doesn't finish.
      $view->execute_time = NULL;

      try {
        $mantis = new mantis(variable_get('mantis_user', 'mantis'), variable_get('mantis_password', 'mantis'), variable_get('mantis_url', 'http://localhost/mantis'));
        $mantis_projects = $mantis->get_projects_user_accessible();
      }
      catch (WSClientException $e) {
        drupal_set_message('Impossible connect to Mantis Bug Tracker. Please check your connection parameter in Administration > Configuration >  Mantis Bug Tracker', 'error');
        return '';
      }
      $result = array();

      foreach ($mantis_projects as $project) {
        // Evaluates if this project has been enabled (in 'query' option) for
        // this view.
        foreach ($this->options['mantis_projects_enabled'] as $id_project => $enabled) {
          if ($enabled && $id_project == $project->id) {
            $result[] = $project;
          }
        }
      }

      if (!empty($this->orderby)) {
        // Array reverse, because the most specific are first.
        // At the moment only order by string fields.
        foreach (array_reverse($this->orderby) as $orderby) {
          $orderby->sort($result);
        }
      }

      // Pager.
      if ($this->pager->use_count_query() || !empty($view->get_total_rows)) {
        $this->pager->total_items = count($result);
        if (!empty($this->pager->options['offset'])) {
          $this->pager->total_items -= $this->pager->options['offset'];
        }

        $this->pager->update_page_info();
      }
      $offset = !empty($this->offset) ? intval($this->offset) : 0;
      $limit = !empty($this->limit) ? intval($this->limit) : 0;
      $result = $limit ? array_slice($result, $offset, $limit) : array_slice($result, $offset);

      $view->result = $result;
      $view->total_rows = count($result);
      $this->pager->post_execute($view->result);
      $view->execute_time = microtime(TRUE) - $start;
    }
    catch(Exception $e) {
      drupal_set_message('An error has been found in execute (project plugin query)');
      return '';
    }
  }

  /**
   * Option definition.
   */
  function option_definition() {
    $options = parent::option_definition();
    try {
      try {
        $mantis = new mantis(variable_get('mantis_user', 'mantis'), variable_get('mantis_password', 'mantis'), variable_get('mantis_url', 'http://localhost/mantis'));
        $mantis_projects = $mantis->get_projects_user_accessible();
      }
      catch (WSClientException $e) {
        drupal_set_message('Impossible connect to Mantis Bug Tracker. Please check your connection parameter in Administration > Configuration >  Mantis Bug Tracker', 'error');
        return $options;
      }
      $options_selected = array();
      foreach ($mantis_projects as $project) {
        $options_selected += array($project->id => $project->name);
      }
      $options['mantis_projects_enabled'] = array('default' => $options_selected);
    }
    catch(Exception $e) {
      drupal_set_message(t('I have troubles in option definition.'), 'error');
    }
    return $options;
  }

  /**
   * Options form. Through this form the users can selects which Mantis
   * projects will be shown in the view.
   */
  function options_form(&$form, &$form_state) {
    try {
      try {
        $mantis = new mantis(variable_get('mantis_user', 'mantis'), variable_get('mantis_password', 'mantis'), variable_get('mantis_url', 'http://localhost/mantis'));
        $mantis_projects = $mantis->get_projects_user_accessible();
      }
      catch (WSClientException $e) {
        return;
      }
      $options = array();
      foreach ($mantis_projects as $project) {
        $options += array($project->id => $project->name);
      }

      $form['mantis_projects_enabled'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Mantis Projects enabled'),
        '#default_value' => array_keys(
          array_filter($this->options['mantis_projects_enabled'], function($elem) {
            return $elem;
          })
        ),
        '#description' => t("Mantis Projects enabled for this view."),
        '#options' => $options,
      );
    }
    catch(Exception $e) {
      drupal_set_message(t('I have troubles in option definition (project plugin query)'), 'error');
      return;
    }
  }

  /**
   * Add Order By.
   */
  function add_orderby($orderby) {
    $this->orderby[] = $orderby;
  }

  function add_where($group, $field, $value = NULL, $operator = NULL) {
  }
}
