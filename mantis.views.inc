<?php
/**
 * Implements hook_views_data().
 */
function mantis_views_data() {
  try {
    $mantis = new mantis(variable_get('mantis_user', 'mantis'), variable_get('mantis_password', 'mantis'), variable_get('mantis_url', 'http://localhost/mantis'));

    $mantis_data_types = $mantis->get_data_types();

    $data = array();

    // I define a group of views called 'Mantis BT projects'. This groups will be
    // used exclusively for to generate Mantis Projects views.
    $data['mantis_projects']['table']['group'] = t('Mantis BT projects');
    $data['mantis_projects']['table']['base'] = array(
      'title' => t('Mantis BT Projects'),
      'help' => t('Fetch projects from Mantis Bug Tracker.'),
      'query class' => 'mantis_project_query',
    );

    // I get info about structure of a "ProjectData" data type.
    $mantis_project_data_types_properties = $mantis_data_types[MANTIS_PROJECT_DATA_TYPE]['property info'];

    // Each component of "ProjectData" data type will be a field in the view.
    foreach ($mantis_project_data_types_properties as $key_props => $value_props) {
      $handler_field = mantis_handler_field_by_data_type();
      $data['mantis_projects'][$key_props] = array(
        'title' => $key_props,
        'help' => t('Data type:') . $value_props['type'],
        'field' => array(
          'handler' => $handler_field[$value_props['type']]['handler_field'],
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => $handler_field[$value_props['type']]['handler_filter'],
        ),
        'sort' => array(
          'handler' => $handler_field[$value_props['type']]['handler_sort'],
        ),
      );
    }

    // I define a group of views called 'Mantis BT issues'. This groups will be
    // Used exclusively for to generate Mantis Issues views.
    $data['mantis_issues']['table']['group'] = t('Mantis BT issues');
    $data['mantis_issues']['table']['base'] = array(
      'title' => t('Mantis BT Issues'),
      'help' => t('Fetch issues from Mantis Bug Tracker.'),
      'query class' => 'mantis_issue_query',
    );

    $mantis_issue_data_types_properties = $mantis_data_types[MANTIS_ISSUE_DATA_TYPE]['property info'];

    foreach ($mantis_issue_data_types_properties as $key_props => $value_props) {
      $handler_field = mantis_handler_field_by_data_type();
      $data['mantis_issues'][$key_props] = array(
        'title' => $key_props,
        'help' => t('Data type:') . $value_props['type'],
        'field' => array(
          'handler' => $handler_field[$value_props['type']]['handler_field'],
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => $handler_field[$value_props['type']]['handler_filter'],
        ),
        'sort' => array(
          'handler' => $handler_field[$value_props['type']]['handler_sort'],
        ),
      );
    }
    return $data;
  }
  catch(Exception $e) {
    drupal_set_message(t('Mantis not configured.'));
  }
}

/**
 * Implements hook_views_plugins().
 */
function mantis_views_plugins() {
  $path = drupal_get_path('module', 'mantis') . '/views/plugins';
  $plugins = array();
  $plugins['query']['mantis_project_query'] = array(
    'title' => t('Mantis Bug Tracker'),
    'help' => t('Plugin for query of view.'),
    'handler' => 'mantis_project_views_plugin_query',
    'path' => $path,
  );
  $plugins['query']['mantis_issue_query'] = array(
    'title' => t('Mantis Bug Tracker'),
    'help' => t('Plugin for query of view.'),
    'handler' => 'mantis_issue_views_plugin_query',
    'path' => $path,
  );
  return $plugins;
}

/**
 * Custom function used for maps handlers fields with mantis data types.
 * So each mantis data type should be associated to  its own handler.
 */
function mantis_handler_field_by_data_type() {
  $handlers = array(
    MANTIS_OBJECTREF_DATA_TYPE => array(
      'handler_field' => 'mantis_object_ref_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_OBJECTREF_DATA_TYPE_ARRAY => array(
      'handler_field' => 'mantis_object_ref_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_ACCOUNT_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_ACCOUNT_DATA_TYPE_ARRAY => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_USER_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_ATTACHMENT_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_ATTACHMENT_DATA_TYPE_ARRAY => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_PROJECT_ATTACMENT_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_RELATIONSHIP_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_RELATIONSHIP_DATA_TYPE_ARRAY => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_ISSUE_NOTE_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_ISSUE_NOTE_DATA_TYPE_ARRAY => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_ISSUE_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_HISTORY_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_ISSUE_HEADER_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_PROJECT_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_PROJECT_DATA_TYPE_ARRAY => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_PROJECT_VERSION_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_FILTER_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_CUSTOM_FIELD_DEFINITION_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_CUSTOM_FIELD_LINK_PROJECT_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_CUSTOM_FIELD_VALUE_ISSUE_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_CUSTOM_FIELD_VALUE_ISSUE_DATA_TYPE_ARRAY => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_TAG_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_TAG_DATA_SEARCH_RESULT_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_PROFILE_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_PROFILE_DATA_SEARCH_RESULT_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_INTEGER_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_TEXT_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_string',
    ),
    MANTIS_BOOLEAN_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
    MANTIS_DATE_TIME_DATA_TYPE => array(
      'handler_field' => 'views_handler_field',
      'handler_filter' => 'mantis_handler_filter_none',
      'handler_sort' => 'mantis_handler_sort_none',
    ),
  );
  return $handlers;
}
